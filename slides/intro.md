# The wonder of open source

---slide---

## Story

---slide---

## History

* 1950s – Academics/the A-2 system
* 1974 - software copyrights
* 1980s - Richard Stallman -> GNU, Free Software Foundation
* 1990s - Linux/Apache HTTP Server/Open Office
* 2000s - Git/Android/Node.js/Docker/Kubernetes

---slide---

## Going strong

>> In 2021, there were 413 million contributions made to the open source projects on GitHub.

* https://www.freecodecamp.org/news/brief-history-of-open-source/

---slide---

## Thank you for

* Contributing
* Reporting issues
* Sharing knowledge
* Be thankful

---slide---

## Making the world a slightly better place